CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Unearth Theme is Mobile-friendly Drupal 9/10 responsive theme.
This theme features are custom Banner with max 5 min 1 card,
Footercopyright field, Social Menu Icon, Contact us Information Fields.


 FEATURES
--------
 * Responsive, Mobile-Friendly Theme
 * In built Font Awesome5
 * Custom Banner card.
 * Custom Banner background.
 * Sticky header.
 * Footer copyright section.
 * Drop Down main menus on hover with 2 level menu.


INSTALLATION
------------

 * Install the Unearth theme as you would normally install a
   contributed Drupal theme. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

Navigate to Administration > Appearance and enable the theme.

Available options in the theme settings:


 * Choose One to five cards on banner.
 * Change banner background image.
 * Change copyright text.
 * Change Social Media Links.

MAINTAINERS
-----------

 * Bindu - https://www.drupal.org/u/bindu-r
