// $(document).ready(function() {
//   $(".owl-carousel").owlCarousel({
//     loop: true,
//     margin: 0,
//     nav: true,
//     items: 1,
//     autoplay: true,
//     autoplayTimeout: 5000,
//     autoplayHoverPause: true
//   });
// });


jQuery(document).ready(function($) {


  var siteCarousel = function() {
    if ($('.nonloop-block-13').length > 0) {
      $('.nonloop-block-13').owlCarousel({
        center: false,
        items: 1,
        loop: true,
        stagePadding: 0,
        margin: 0,
        smartSpeed: 1000,
        autoplay: true,
        nav: true,
        navText: ['<span class="icon-arrow_back">', '<span class="icon-arrow_forward">'],
        responsive: {
          600: {
            margin: 0,
            nav: true,
            items: 2
          },
          1000: {
            margin: 0,
            stagePadding: 0,
            nav: true,
            items: 2
          },
          1200: {
            margin: 0,
            stagePadding: 0,
            nav: true,
            items: 3
          }
        }
      });
    }

    $('.slide-one-item').owlCarousel({
      center: false,
      items: 1,
      loop: true,
      stagePadding: 0,
      margin: 0,
      smartSpeed: 1500,
      autoplay: true,
      pauseOnHover: false,
      dots: true,
      nav: true,
      navText: ['<span class="icon-keyboard_arrow_left">', '<span class="icon-keyboard_arrow_right">']
    });
  };
  siteCarousel();


});
