$(document).ready(function() {
  var navbar = $('.navbar');
  var topBarHeight = 0;

  if ($('body').hasClass('user-logged-in')) {
    topBarHeight = $('.top-bar').outerHeight();
  }

  var adminToolbarHeight = 43;
  var managebarHeight = 35;

  $(window).scroll(function() {
    var scrollTop = $(this).scrollTop();
    var offset = topBarHeight;

    if ($('body').hasClass('toolbar-tray-open')) {
      offset += adminToolbarHeight;
    }
    if ($(window).width() > 768) {
      if ($('nav').hasClass('toolbar-bar')) {
        offset += managebarHeight;
      }
    }

    if ($('nav').hasClass('not-front')) {
      if (scrollTop > offset) {
        navbar.addClass('sticky shadow-sm').css('top', offset + 'px');
        $('.nav-new').css({
          'width': '',
          'position': '',
          'z-index': ''
        });
        $('.nav-new').css('background-color', 'white');
        removeRegularStyles();
      } else {
        navbar.removeClass('sticky shadow-sm').css('top', '');
        $('.nav-new').css({
          'width': '100%',
          'position': 'absolute',
          'z-index': '10'
        });
        $('.nav-new').css('background-color', '');
        removeRegularStyles();
      }
    } else if ($('nav').hasClass('front')) {
      if (scrollTop > offset) {
        navbar.addClass('sticky shadow-sm').css('top', offset + 'px');
        $('.nav-new').css({
          'width': '',
          'position': '',
          'z-index': ''
        });
        $('.nav-new').css('background-color', 'white');
        removeRegularStyles();
      } else {
        navbar.removeClass('sticky shadow-sm').css('top', '');
        $('.nav-new').css({
          'width': '100%',
          'position': 'absolute',
          'z-index': '10'
        });
        $('.nav-new').css('background-color', '');
        restoreRegularStyles();
      }
    }
  });

  function removeRegularStyles() {
    $('header .menu-item-0').mouseenter(function() {
      $(this).css('color', '#ffb800');
    });

    $('header .menu-item-0').mouseleave(function() {
      $(this).css('color', 'black');
    });
    $('header ul.menu-level-0 li a').css('color', 'black');
    $('header ul.menu-level-0 li a').css('font-weight', 'bold');

    $('.banner-wrap').css('position', '');
    $('.navbar-brand a').css('color', '');
    $('.site-title').css('color', '');
  }

  function restoreRegularStyles() {
    $('header .menu-item-0').mouseenter(function() {
      $(this).css('color', '#ffb800');
    });

    $('header .menu-item-0').mouseleave(function() {
      $(this).css('color', 'white');
    });
    if ($(window).width() > 768) {
      $('header ul.menu-level-0 li a').css('color', 'white');
    }
    $('header ul.menu-level-1 li a,ul.menu-level-2 li a').css('color', 'black');
    $('.banner-wrap').css('position', 'relative');
    $('.navbar-brand a').css('color', 'white');
    $('.site-title').css('color', 'white');
  }

  $(window).scroll();

  $(window).resize(function() {
    if ($(window).width() <= 768) {
      topBarHeight = 0;
    } else {
      if ($('body').hasClass('user-logged-in')) {
        topBarHeight = $('.top-bar').outerHeight();
      }
    }
    if ($(window).width() <= 768) {
      $('header ul.menu-level-0').css({
        'position': 'absolute',
        'background-color': 'white',
        'right': '0',
        'top': '50',
        'flex-direction': 'column',
        'width': '170px'
      });
    }
  });
});

window.addEventListener('DOMContentLoaded', function() {
  var menuItems = document.querySelectorAll('.menu-item-0');
  menuItems.forEach(function(item) {
    if (item.querySelector('.menu-level-1')) {
      item.classList.add('has-child');
    }

    $(document).ready(function() {
      if ($(window).width() <= 768) {
        $('header ul.menu-level-0 li a').css('color', 'black');
      }
    });
  });
});

var navbarToggler = document.querySelector('.navbar-toggler');
var menu = document.querySelector('ul.menu-level-0');

navbarToggler.addEventListener('click', function() {
  menu.classList.toggle('active');
});

if ($(window).width() <= 768) {
  $('header ul.menu-level-0.active').css('display', 'flex');
}


